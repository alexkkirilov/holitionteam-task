export type YearCtrlBtnTypes = 'prev' | 'curr' | 'next';
export type MonthTitleTypes = 'none' | 'short' | 'long';

export interface RouterLink {
    readonly icon?: string;
    readonly label: string;
    readonly link: string;
}
