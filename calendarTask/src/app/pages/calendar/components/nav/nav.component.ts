import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RouterLink } from 'src/app/core/interfaces/default';

@Component({
  selector: 'task-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  public routeLinks: Array<RouterLink> = [
    { label: 'Day', link: '/calendar/day' },
    { label: 'Week', link: '/calendar/week' },
    { label: 'Month', link: '/calendar/month' },
    { label: 'Year', link: '/calendar/year' },
  ];

  constructor(
    public router: Router,
  ) { }


}
