import { Component, Input, OnChanges } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MonthTitleTypes, YearCtrlBtnTypes } from 'src/app/core/interfaces/default';
import { CalendarService } from '../../services/calendar.service';

@Component({
  selector: 'task-monthly-table',
  templateUrl: './monthly-table.component.html',
  styleUrls: ['./monthly-table.component.scss']
})
export class MonthlyTableComponent implements OnChanges {
  // TODO: Clear duplicated code from here and Year page
  @Input() title: MonthTitleTypes = 'none';
  @Input() month: number = this.service.currentMonth;
  @Input() year: number = this.service.currentYear;

  public flow = -1;
  public counter = 0;
  public boundary = Array(6).fill(0).map((x, i) => i);
  public cols: Observable<any[]> = of(this.service.weekDays);

  public currentMonthDays: Date[] = [];
  public prevMonthDays: Date[] = [];
  public nextMonthDays: Date[] = [];

  constructor(
    public service: CalendarService
  ) { }

  ngOnChanges(): void {
    const serviceData: any = this.service.getMonth(this.month, this.year);
    this.prevMonthDays = serviceData.prevMonthDays;
    this.nextMonthDays = serviceData.nextMonthDays;
    this.currentMonthDays = serviceData.currentMonthDays;
    const size = this.currentMonthDays[0].getDay() == 0
      ? (this.currentMonthDays.length + this.nextMonthDays.length) / 7
      : (this.currentMonthDays.length + this.prevMonthDays.length + this.nextMonthDays.length) / 7;
    this.boundary = Array(size).fill(0).map((x, i) => i);
    this.cols = of(this.service.weekDays);
  }

  get styleClass(): string {
    return (this.flow == -1) ? this.isWeekend('prev') ? 'prev-month weekend' : 'prev-month'
      : (this.flow == 1) ? this.isWeekend('next') ? 'next-month weekend' : 'next-month'
        : this.isToday ? 'today'
          : this.isWeekend() ? 'weekend'
            : ''

  }

  isWeekend(type: YearCtrlBtnTypes = 'curr'): boolean {
    if ( // TODO: Some where data is not updated on time and creates that issue - find and fix it
      (type === 'next' && this.nextMonthDays.length <= this.counter) ||
      (type === 'prev' && this.prevMonthDays.length <= this.counter)
    ) type = 'curr';

    const dates = type === 'curr' ? this.currentMonthDays : type === 'next' ? this.nextMonthDays : this.prevMonthDays;
    if (!dates || !dates.length || !dates[this.counter])
      return false;
    const day = dates[this.counter].getDay();
    return day === 0 || day === 6;
  }

  get isToday(): boolean {
    const d1 = new Date(this.currentMonthDays[this.counter]);
    const d2 = new Date();
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getDate() === d2.getDate() &&
      d1.getMonth() === d2.getMonth();
  }


  public getTDVal(row: number, col: number) {
    if (!row && !col) {
      this.flow = -1;
      this.counter = 0;
    }

    let day;

    // Append previous month days
    if (this.flow == -1 && this.counter < this.prevMonthDays.length) {
      // fix the flow if current month's starting day is Sat
      if (this.currentMonthDays[0].getDay() == 0) {
        this.flow++;
        this.counter = 0;
        col--;
      }

      day = this.prevMonthDays[this.counter].getDate();
    }

    // Append current month days
    if (this.flow == 0 && this.counter < this.currentMonthDays.length)
      day = this.currentMonthDays[this.counter].getDate();

    // Append next month days
    if (this.flow == 1 && this.counter < this.nextMonthDays.length)
      day = this.nextMonthDays[this.counter].getDate();

    this.counter++;

    // Control the flow
    if (
      (this.flow == -1 && this.counter == this.prevMonthDays.length)
      || (this.flow == 0 && this.counter == this.currentMonthDays.length)
    ) {
      this.flow++;
      this.counter = 0;
    }

    return day;
  }
}
