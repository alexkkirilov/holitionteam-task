import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MonthTitleTypes } from 'src/app/core/interfaces/default';
import { CalendarService } from '../../services/calendar.service';

@Component({
  selector: 'task-yearly-table',
  templateUrl: './yearly-table.component.html',
  styleUrls: ['./yearly-table.component.scss']
})
export class YearlyTableComponent implements OnInit, OnChanges {

  @Input() title: MonthTitleTypes = 'none';
  @Input() month: number = this.service.currentMonth;
  @Input() year: number = this.service.currentYear;

  public monthName = new Date(+this.service.currentYear, +this.service.currentMonth);

  public flow = -1;
  public counter = 0;
  public boundary = Array(6).fill(0).map((x, i) => i);

  public currentMonthDays: Date[] = [];
  public prevMonthDays: Date[] = [];
  public nextMonthDays: Date[] = [];

  constructor(
    public service: CalendarService
  ) { }

  ngOnInit(): void {
    window.scroll(0, 1);
  }

  ngOnChanges(): void {
    const serviceData: any = this.service.getMonth(this.month, this.year);
    this.monthName = new Date(+this.year, +this.month - 1);

    this.prevMonthDays = serviceData.prevMonthDays;
    this.nextMonthDays = serviceData.nextMonthDays;
    this.currentMonthDays = serviceData.currentMonthDays;
    const size = this.currentMonthDays[0].getDay() == 0
      ? (this.currentMonthDays.length + this.nextMonthDays.length) / 7
      : (this.currentMonthDays.length + this.prevMonthDays.length + this.nextMonthDays.length) / 7;
    this.boundary = Array(size).fill(0).map((x, i) => i);
  }

  get styleClass(): string {
    return (this.flow == -1) ? 'prev-month'
      : (this.flow == 1) ? 'next-month'
        : this.isToday ? 'today'
          : this.isWeekend ? 'weekend'
            : ''

  }

  get isWeekend(): boolean {
    const day = this.currentMonthDays[this.counter].getDay();
    return day === 0 || day === 6;
  }

  get isToday(): boolean {
    const d1 = new Date(this.currentMonthDays[this.counter]);
    const d2 = new Date();
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getDate() === d2.getDate() &&
      d1.getMonth() === d2.getMonth();
  }

  public getTDVal(row: number, col: number) {
    if (!row && !col) {
      this.flow = -1;
      this.counter = 0;
    }

    let day;

    // Append previous month days
    if (this.flow == -1 && this.counter < this.prevMonthDays.length) {
      // fix the flow if current month's starting day is Sat
      if (this.currentMonthDays[0].getDay() == 0) {
        this.flow++;
        this.counter = 0;
        col--;
      }

      day = this.prevMonthDays[this.counter].getDate();
    }

    // Append current month days
    if (this.flow == 0 && this.counter < this.currentMonthDays.length)
      day = this.currentMonthDays[this.counter].getDate();

    // Append next month days
    if (this.flow == 1 && this.counter < this.nextMonthDays.length)
      day = this.nextMonthDays[this.counter].getDate();

    this.counter++;

    // Control the flow
    if (
      (this.flow == -1 && this.counter == this.prevMonthDays.length)
      || (this.flow == 0 && this.counter == this.currentMonthDays.length)
    ) {
      this.flow++;
      this.counter = 0;
    }

    return day;
  }
}
