import { Component, HostListener, OnInit } from '@angular/core';
import { BehaviorPropsService } from './services/behavior-props.service';

@Component({
  selector: 'task-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  public getScreenWidth: any;
  public getScreenHeight: any;

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    // TODO: Update the screen size requirements if need different mobile screen size
    // Check the sizes from the mixins.scss file to match the other screen requirements
    this.deviceService.updateDeviceSize(window.innerWidth <= 640)
  }

  
  constructor(
    private deviceService: BehaviorPropsService
  ) { }

  ngOnInit(): void {
  }

}
