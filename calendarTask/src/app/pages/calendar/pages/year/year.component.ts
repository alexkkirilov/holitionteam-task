import { take } from 'rxjs';
import { Location } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { YearCtrlBtnTypes } from 'src/app/core/interfaces/default';
import { CalendarService } from '../../services/calendar.service';
import { BehaviorPropsService } from '../../services/behavior-props.service';

@Component({
  selector: 'task-year',
  templateUrl: './year.component.html',
  styleUrls: ['./year.component.scss']
})
export class YearComponent implements OnInit {

  public year = this.service.currentYear;
  public months = Array(12).fill(0).map((x, i) => i + 1);

  public years: any[] = [this.year];
  public isMobile: boolean = false;

  private lastScrollTop = 2;
  private yearCalendarHeight: number = 0;
  private scrollMoving: boolean = false;
  private moveScrollToPosition: number = 0;

  @ViewChild('yearList') yearList: ElementRef | any;
  @ViewChild('yearContainer') yearContainer: ElementRef | any;

  @HostListener("window:scroll", ["$event"])
  onScroll(event: any) {
    if (this.isMobile) {
      let st = window.pageYOffset || document.documentElement.scrollTop;

      // Prevent any actions until the scroll moves to the correct position
      if (this.scrollMoving) return;

      if (st > this.lastScrollTop && (document.documentElement.scrollHeight - window.innerHeight - document.documentElement.scrollTop) < 30) {
        // down scroll code
        this.moveScrollToPosition = st - 30
        this.scrollMoving = true;
        this.moveScroll();
        this.loadNextYear();
      } else if (st < 30) {
        // up scroll code
        this.moveScrollToPosition = st + this.yearCalendarHeight;
        this.scrollMoving = true;
        this.moveScroll();
        this.loadPrevYear();
      }

    }
  }

  constructor(
    private location: Location,
    private service: CalendarService,
    private activatedRoute: ActivatedRoute,
    public propService: BehaviorPropsService,
  ) {
    propService.isDeviceMobile.subscribe(bool => {
      this.isMobile = bool;
      this.yearCalendarHeight = this.yearList?.nativeElement?.offsetHeight || 0;

      if (!bool) { // Set initial state
        this.years = [this.year];
        if (this.lastScrollTop !== 2)
          this.lastScrollTop = 2;
      } else if (this.years.length === 1) {
        // Do the magic and load the previous and next year details
        this.years = [this.year - 1, this.year, this.year + 1];
        this.lastScrollTop = this.yearCalendarHeight;
        this.moveScrollToPosition = this.yearCalendarHeight;
        this.moveScroll();
      }

    })
  }

  private moveScroll() {
    // TODO: think for a better way
    // Idea is to ensure the UI is already updated before moving to the current year
    setTimeout(() => { window.scroll(0, this.moveScrollToPosition); this.scrollMoving = false;}, 300);
  }

  private updateURL(year: number) {
    this.location.replaceState(`/calendar/year/${year}`);
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(take(1)).subscribe(({ year }) => {
      (year) ? this.year = year : this.updateURL(this.service.currentYear);
    });
  }


  public changeYear(btn: YearCtrlBtnTypes) {
    switch (btn) {
      case 'prev': this.year = +this.year - 1; break;
      case 'next': this.year = +this.year + 1; break;
      case 'curr': this.year = this.service.currentYear; break;
    }

    this.updateURL(this.year);
  }


  public loadPrevYear() {
    this.years.unshift(+this.years[0] - 1);
  }

  public loadNextYear() {
    this.years.push(+this.years[this.years.length - 1] + 1)
  }
}
