import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { YearComponent } from './year.component';
import { YearlyTableComponent } from '../../components/yearly-table/yearly-table.component';

const routes: Routes = [
  { path: '', component: YearComponent },
  { path: '/:year', component: YearComponent },
];


@NgModule({
  declarations: [
    YearComponent,
    YearlyTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ]
})
export class YearModule { }
