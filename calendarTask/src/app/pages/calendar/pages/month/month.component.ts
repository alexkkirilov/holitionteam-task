import { take } from 'rxjs';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { YearCtrlBtnTypes } from 'src/app/core/interfaces/default';
import { CalendarService } from '../../services/calendar.service';

@Component({
  selector: 'task-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss']
})
export class MonthComponent implements OnInit {
  public year = this.service.currentYear;
  public month = this.service.currentMonth;
  public date = new Date(this.year, this.month);
  public months = Array(12).fill(0).map((x, i) => i + 1);

  public monthName = new Date(+this.service.currentYear, +this.service.currentMonth);

  constructor(
    private location: Location,
    private service: CalendarService,
    private activatedRoute: ActivatedRoute,
  ) { }

  private updateURL(
    year: number = this.service.currentYear,
    month: number = this.service.currentMonth
  ) {
    this.location.replaceState(`/calendar/month/${year}/${month}`);
    this.monthName = new Date(+this.year, +this.month - 1);
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(take(1)).subscribe(({ year, month }) => {
      this.year = +year || this.service.currentYear;
      this.month = (month && month <= 12 && month > 0) ? +month : this.service.currentMonth
      this.updateURL(this.year, this.month);
    });
  }

  public changeMonth(btn: YearCtrlBtnTypes) {
    switch (btn) {
      case 'prev':
        if (+this.month == 1) {
          this.month = 12;
          this.year = +this.year - 1;
        } else this.month = +this.month - 1;
        break;
      case 'next':
        if (+this.month == 12) {
          this.month = 1;
          this.year = +this.year + 1;
        } else this.month = +this.month + 1;
        break;

      case 'curr':
        this.month = this.service.currentMonth;
        this.year = this.service.currentYear;
        break;
    }

    this.updateURL(this.year, this.month);
  }
}
