import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { MonthComponent } from './month.component';
import { MonthlyTableComponent } from '../../components/monthly-table/monthly-table.component';

const routes: Routes = [
  { path: '', component: MonthComponent },
  { path: '/:year', component: MonthComponent },
  { path: '/:year/:month', component: MonthComponent }
];

@NgModule({
  declarations: [
    MonthComponent,
    MonthlyTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ]
})
export class MonthModule { }
