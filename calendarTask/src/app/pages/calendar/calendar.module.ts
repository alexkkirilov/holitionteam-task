import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Components
import { CalendarComponent } from './calendar.component';
import { NavComponent } from './components/nav/nav.component';
import { HeaderComponent } from './components/header/header.component';
import { BehaviorPropsService } from './services/behavior-props.service';

const routes: Routes = [
  {
    path: '', component: CalendarComponent, children: [
      { path: '', pathMatch: 'full', redirectTo: `year` },
      { path: 'year', loadChildren: () => import('./pages/year/year.module').then(m => m.YearModule) },
      { path: 'month', loadChildren: () => import('./pages/month/month.module').then(m => m.MonthModule) },
      { path: 'week', loadChildren: () => import('./pages/week/week.module').then(m => m.WeekModule) },
      { path: 'day', loadChildren: () => import('./pages/day/day.module').then(m => m.DayModule) }
    ]
  }
]

@NgModule({
  declarations: [
    CalendarComponent,
    HeaderComponent,
    NavComponent,   
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    BehaviorPropsService
  ]
})
export class CalendarModule { }
