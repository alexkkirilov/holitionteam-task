import { Injectable } from '@angular/core';
import { BehaviorPropsService } from './behavior-props.service';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  currentDate = new Date();
  currentMonth = this.currentDate.getMonth() + 1;
  currentYear = this.currentDate.getFullYear();

  public readonly weekDays = ['s', 'm', 't', 'w', 't', 'f', 's'];
  public readonly weekDaysShort = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
  // public readonly weekDaysMonday = ['m', 't', 'w', 't', 'f', 's', 's'];
  // public readonly weekDaysSunday = ['s', 'm', 't', 'w', 't', 'f', 's'];

  constructor(
    private obs: BehaviorPropsService
  ) { }

  private getCurrentMonthDates(date: Date) {
    const currentMonthDays = [];
    const currentMonth = new Date(date);
    currentMonth.setDate(1);
    let temp = new Date(currentMonth);
    temp.setMonth(currentMonth.getMonth() + 1);
    temp.setDate(0);
    let stop = temp.getDate();
    while (true) {
      currentMonthDays.push(new Date(currentMonth));
      if ((currentMonth.getDate() == stop))
        break;
      currentMonth.setDate(currentMonth.getDate() + 1);
    }

    return currentMonthDays;
  }

  private getPrevMonthDates(date: Date) {
    const prevMonthDays = [];
    const prevMonth: any = new Date(date);
    prevMonth.setDate(0);
    while (true) {
      if (!this.obs.isFromMonday) prevMonthDays.push(new Date(prevMonth));
      if (prevMonth.getDay() == 0)
        break;
      if (this.obs.isFromMonday) prevMonthDays.push(new Date(prevMonth));
      prevMonth.setDate(prevMonth.getDate() - 1);
    };
    prevMonthDays.reverse();
    return prevMonthDays;
  }

  private getNextMonthDates(date: Date) {
    const nextMonthDays = [];
    const nextMonth = new Date(date);
    nextMonth.setMonth(nextMonth.getMonth() + 1);
    nextMonth.setDate(1);
    while (true) {
      if (this.obs.isFromMonday) nextMonthDays.push(new Date(nextMonth));
      if (nextMonth.getDay() == 0)
        break;
      if (!this.obs.isFromMonday) nextMonthDays.push(new Date(nextMonth));

      nextMonth.setDate(nextMonth.getDate() + 1);
    };

    return nextMonthDays;
  }

  public getMonth(month: number, year: number): object {
    const date = new Date(year, month - 1);
    return {
      currentMonthDays: this.getCurrentMonthDates(date),
      prevMonthDays: this.getPrevMonthDates(date),
      nextMonthDays: this.getNextMonthDates(date)
    }
  }
}
