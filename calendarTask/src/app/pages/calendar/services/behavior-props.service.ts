import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

const IsFromMonday = false;

@Injectable({
  providedIn: 'root'
})
export class BehaviorPropsService {
  public isFromMonday: boolean = IsFromMonday;

  private startFromMondaySource = new BehaviorSubject<boolean>(IsFromMonday);
  startFromMonday = this.startFromMondaySource.asObservable();

  changeStartingDay(bool: boolean): void {
    this.isFromMonday = bool
    this.startFromMondaySource.next(bool);
  }

  private isDeviceHAsMobileSizeSource = new BehaviorSubject<boolean>(false);
  isDeviceMobile = this.isDeviceHAsMobileSizeSource.asObservable();

  updateDeviceSize(bool: boolean): void {
    this.isDeviceHAsMobileSizeSource.next(bool);
  }
}
