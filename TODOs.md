# App Remaining tasks
* Improve the style for the mobiles view to look a like the task images - ( Partly done )
* Hide Prev and Next month dates for all pages for mobile view - ( Partly done )
* Style to look a like more as task image for the input field
* Scrollable page for mobile views and dynamically load the prev/next month/year ( Partly done - Year only)

# App Evolution
* Install localbase library
* Add localbase service which handles the IndexedDB requests
* Implement and connect with (add button in header component) to store user notes
* Connect the stored notes with the currentMonth date lists and add indicators
* Display notes for specific day in day page

# App Bugs
* Monthly page - isWeekend function has an identifier issue which is not updated correctly into the service - requires extra attention

# App Code clean
* Year and Month pages have similar/identical code - need to be moved to a service and reduce the code into the components